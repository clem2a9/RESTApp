"use strict";
var serveur = 'http://etudiant12.chezmeme.com/rest/v1/web/mesfrais/';
var serveurId = "http://chezmeme.com/sso/api/v1.5/";

sessionStorage.setItem("token", localStorage.getItem('token')); 
var token= sessionStorage.getItem("token");
$(document).ready(function () {
    console.log(localStorage.getItem('token'));
    $("#page").hide();
    $("#table").hide();
    $("#welcome").show();
    $("#creation").hide();
    $("#alert").hide();
    sessionStorage.removeItem('numnote');
    $("#envoi").click(ajoutnote);
    console.log( sessionStorage.getItem("token"));
    $("#portal").click(portal);
    console.log(token);
    modiFrais();
    if (!token || token==="null") {
        console.log("test");
        
        window.location.href = '../../index.html';
    } else if (token) {
        $.get(serveurId + "check_access/" + token + "/notes", function (data) {
            if (data['ret'] === "granted") {
                whois();
            } else if (data['ret'] === "expired") {
                sessionStorage.removeItem('id');
                sessionStorage.removeItem('token');
                token=null;
                window.location.href = '../../index.html';
            }
        }).fail(function () {
            alert("Problème de connexion !");
        });
    }
    $("#ajouter").click(showajout);
    $("#rechercher").click(showpage);

    $("#searchButton").on('click', search);
});
function portal(evt){
    window.location.href = '../../index.html';
}
   /**
   * whois function permettant de récupérer les données d'un utilisateur grâce avec token et de le ajouter dans input user
   * @param token
   * @return id 
   */
function whois(event) {
    //var token = sessionStorage.getItem("token");
    if (token != undefined) {

        $.get(serveurId + "who_is/" + token, function (data) {
            if (data["ret"] != 0) {
                $("#user").html('<option value="' + data["id"] + '">' + data["nom"] + " " + data["prenom"] + '</option>');
                sessionStorage.setItem("id", data["id"]);
            } else if (data["ret"] == 0) {
                expired();

            }
        });
    } else {
        expired();
    }
}
  
   /**
   * search function permet d'afficher les notes  dans une periode saissie par utilisateur
   * @param date
   * @return  
   */

function search(event) {
    var id = sessionStorage.getItem("id");
    var number = 0;

    if ($("#dateDebut").val() !== "" || $("#dateFin").val() !== "") {
        $("#tableRows").show();
        $("#table tbody tr").each((e, index) => {
            var id = index.id;
            var value = index.innerText;
            if (value <= $("#dateDebut").val() || value >= $("#dateFin").val() + 1) {
                $('#' + id).hide();
            } else {
                $('#' + id).show();
                number++;
            }
        })
        if (number == 0) {
            message("aucune");
        }
    } else {
        message("date");
    }
}
var num;
   /**
   * showajout function affiche la parti ajout de l'application
   * @param id (dans le cas que la note existe)
   * @return  
   */
function showajout(id) {

    sessionStorage.removeItem('numnote');
    $("#envoi").show();
    $("#welcome").hide();
    num = 1;
    $('#montant').val("");
    $('#date').val("");
    $('#commentaire').val("");
    $('#gridCheck').prop('checked', false)
    $('#date').prop('readonly', false);
    $('#commentaire').prop('readonly', false);
    $('#gridCheck').prop('disabled', false);
    $('#date').prop('readonly', false);
    $('.test tr').remove();
    whois();
    $("#page").hide();
    $("#creation").show();
    $("#welcome").hide();
    $("#tableRows").html("");
    $("#table").show();
    $("#tableRows").hide();
    if (isNaN(id) == false) {
        modifnote(id);
        $("#envoi").hide();
        sessionStorage.setItem("numnote", id);
    }
}
   /**
   * modifnote function permet de modifier affichage dans le cas que la note est verrouiller et afficher le contenu du note 
   * @param id 
   * @return  
   */

function modifnote(id) {
   // var token = sessionStorage.getItem("token");
    $.get(serveur + "notes/" + token + "/" + id, function (data) {
        data.forEach(function (element, index) {
            $('#commentaire').val(element.note_comment);
            $('#date').val(element.note_date);
            if (element.note_verrou == 1) {
                $('#commentaire').prop('readonly', true);
                $('#date').prop('readonly', true);
                $("#gridCheck").prop('checked', true);
                $('#gridCheck').prop('disabled', true);
            } else {
                $('#commentaire').prop('readonly', false);
                $('#date').prop('readonly', false);
                $('#gridCheck').prop('disabled', false);
            }
            $.get(serveur + "notefrais/" + token + "/" + id, function (data) {
                var totaux = parseFloat(0);
                data.forEach(function (element2, index) {
                    if (data != "Pas de frais") {
                        if (element.note_verrou == 0) {
                            $(".test").append('<tr class="' + element2['frais_id'] + '"><th class="hidden-xs" scope="row">' + num +
                                '</th><td><select class="form-control col-md-7 col-lg-7 formnote typedenote" id="type" name="type"></select></td><td><input type="number" min="0.01" step="0.01"   class="form-control formnote montantnote" id="montantnote" value="' + element2['frais_montant'] + '" /></td><td><input  class="form-control formnote commentaire" id="commentairenote" value="' + element2["frais_comment"] + '"/></td> <td ><img src="images/bleach-sign.svg" id="' + element2['frais_id'] + '"  class="delete"/></td></tr>');
                            totaux = totaux + parseFloat(element2['frais_montant']);


                        } else if (element.note_verrou == 1) {
                            $(".test").append('<tr class="' + element2['frais_id'] + '"><th class="hidden-xs" scope="row">' + num +
                                '</th><td><select class="form-control col-md-7 col-lg-7 formnote typedenote" id="type" name="type" readonly></select></td><td><input type="number" min="0.01" step="0.01"   class="form-control formnote montantnote" id="montantnote" value="' + element2["frais_montant"] + '" readonly/></td><td><input  class="form-control formnote commentaire" id="commentairenote" value="' + element2["frais_comment"] + '" readonly/></td> <td ><img src="images/bleach-sign.svg" id="' + element2['frais_id'] + '"  class=""/></td></tr>');
                            totaux = totaux + parseFloat(element2['frais_montant']);
                        }
                        cal();

                        //var token = sessionStorage.getItem("token");
                        $.get(serveur + "types/" + token, function (data) {
                            if (data != "expired") {
                                data.forEach(function (val) {
                                    if (val["type_id"] == element2["frais_id_type"]) {
                                        $("." + element2['frais_id'] + " select ").append('<option value="' + val["type_id"] + '" selected>' + val["type_nom"] + '</option>');
                                    } else {
                                        $("." + element2['frais_id'] + " select ").append('<option value="' + val["type_id"] + '">' + val["type_nom"] + '</option>');
                                    }
                                })
                            } else {
                                expired();
                            }
                        });
                        modiFrais();
                        num++;
                    }

                });
                $("#montant").html(totaux);
                if (element.note_verrou == 0) {
                    ajoutline();
                }
            });


        })
    })
}
   /**
   * showpage function permet d'afficher la page de recherche
   * @param  token et id  
   */

function showpage() {
    $(".test tr").remove();
    sessionStorage.removeItem('numnote');
    //var token = sessionStorage.getItem("token");
    var id = sessionStorage.getItem("id");
    $("#page").show();
    $("#creation").hide();
    $("#welcome").hide();
    $("#tableRows").html("");
    $("#table").show();
    $("#tableRows").hide();
    $.get(serveur + 'usernotes/' + token + '/' + id, function (data) {
        if (data["ret"] === 'Pas de notes') {
            $('#tableRows').append('<tr><td>Pas de note pour ce salarié</td><td>');
        } else if (data["ret"] === false) {
            expired();
        } else {
            data.forEach(function (element, index) {
                $("#tableRows").append("<tr id='" + element.note_date + index + "'><td>" + element.note_date + "</td><td><a href='#' onclick='getNote(" + element.note_id + ")'><img src='images/7467-200.png' alt='eyes'/></a></td></tr>");
            })
        }
    }).fail(function () {
        expired();
    })
}

function getNote(id) {
    showajout(id);
}
   /**
   * ajouttype function permet de récupérer la liste des types de frais 
   * @param  token
   * @return  idtype nomtype
   */

function ajouttype() {
    $(".type option").remove();
    //var token = sessionStorage.getItem("token");
    $.get(serveur + "types/" + token, function (data) {
        if (data != "expired") {
            data.forEach(function (val) {
                $(".type").append('<option value="' + val["type_id"] + '">' + val["type_nom"] + '</option>');
            })
        } else {
            expired();
        }
    });
}
var montant;
var com;
   /**
   * modiFrais function permet s'il y a un changement das les input et qu'une note existe alors cela modifie la note et permet de determiner si c'est ajout ou update de frais
   * @param  idnote
   */

function modiFrais() {
        $("#commentaire").change(function () {
           ;
    var note=sessionStorage.getItem("numnote");
            
    if(note != undefined){
            updatenotes();
    }
        
    });   $("#date").change(function () {
            
    var note=sessionStorage.getItem("numnote");
                 
    if(note != undefined){
      
            updatenotes();
    }
        
    });  $("#gridCheck").change(function () {
            
    var note=sessionStorage.getItem("numnote");
                 
    if(note != undefined){
      
            updatenotes();
    }
        
    });
        
    

    $(".typedenote").change(function () {
        var id = $(this).parent().parent().attr("class");
        if (id != undefined) {
            console.log("ici");
            updatefrais($(this));
        }
    });
    $(".montantnote").change(function () {
        
        console.log("ici3");
        var id = $(this).parent().parent().attr("class");
        if (id == undefined) {
            console.log("ici2");
            montant = true;
            if (montant == true && com == true) {
                ajoutfrais($(this));
                
            }
        } else {
            updatefrais($(this));
        }
    });
    $(".commentaire").change(function () {
        var id = $(this).parent().parent().attr("class");
        if (id == undefined) {
            com = true;
            if (montant == true && com == true) {
                ajoutfrais($(this));
    
            }
        } else {
            updatefrais($(this));
        }
    });
    $(".delete").click(suprimer);
}
   /**
   * updatefrais function permet de modifier des frais
   * @param  val 
   * @return  message
   */

function updatefrais(val) {
    console.log("c");
    var line = val.parent().parent();
    var fraismontant =line.find(".montantnote").val();
    var test =fraismontant.match('[-+]?([0-9]*.[0-9]+|[0-9]+)');
    if(test == null){
        fraismontant=undefined;
    }
    var fraiscommentaire = line.find(".commentaire").val();
    var fraistype = line.find(".typedenote").val();
    var nb_note = sessionStorage.getItem("numnote");
    //var token = sessionStorage.getItem("token");
    var idfrais = line.attr("class");
    var id = sessionStorage.getItem("id");
    var nb_note = sessionStorage.getItem("numnote");
    if(fraismontant != undefined){
        $.ajax({
            method: "PUT",
            url: serveur + "frais/" + token + "/" + idfrais + "?commentFrais=" + fraiscommentaire + "&montantFrais=" + fraismontant + "&idNote=" + nb_note + "&idType=" + fraistype
        })
        .done(function (msg) {
            if (msg["ret"] == false) {
                expired();
            } else {
                message("modiffrais");
                cal();
            }
        });   
    }
}

   /**
   * cal function permet de calculer le montant total des frais de la note
   * @param  token et id 
   */


function cal(evt) {
   // var token = sessionStorage.getItem("token");
    var id = sessionStorage.getItem("numnote");
    $.get(serveur + "notefrais/" + token + "/" + id, function (data) {
        var totaux = parseFloat(0);
        data.forEach(function (element2, index) {
            totaux = totaux + parseFloat(element2['frais_montant']);
        });
        $("#montant").val(totaux + " €");
        var total = $("#montant").val();
        if (total == "NaN €") {
            $("#montant").val("0 €");
        }
    });
}

   /**
   * message function permet de afficher un message quand event est succès ou non sucsès
   * @param  msg 
   */

function message(msg) {
    console.log("testt");
    if (msg == "ajoutnote") {
        $("#message").html("Note ajouté");
        $("#message").show();
    }
    if (msg == "sup") {
        $("#message").html("Frais supprimé");
        $("#message").show();
    }
    if (msg == "ajoutfrais") {
        $("#message").html("Frais ajouté");
        $("#message").show();
    }
    if (msg == "modifnote") {
        $("#message").html("Note modifé");
        $("#message").show();
    }
    if (msg == "modiffrais") {
        $("#message").html("Frais modifé");
        $("#message").show();
    }
    if (msg == "oublie") {
        $("#messagewarning").html("Vous avez oubliez de donner la date ou le commentaire !");
        $("#messagewarning").show();
    }
    if (msg == "date") {
        $("#warning").html("Vous devez remplir les champs de date pour effectuer une recherche !");
        $("#warning").show();
    }
    if (msg == "aucune") {
        $("#warning").html("Vous n'avez pas de note à cette periode !");
        $("#warning").show();
    }
    setTimeout(function () {
        $("#message").hide();
        $("#messagewarning").hide();
        $("#warning").hide();
    }, 8500);


}
   /**
   * ajoutnote function permet d'envoyé une note
   * @param  msg 
   */
function ajoutnote() {
    var ver = $('#gridCheck:checked').val();
    if (ver) {
        ver = 1;
    } else {
        ver = 0;
    }
    var date =$('#date').val();
    var commentaire = $('#commentaire').val();
    //var token = sessionStorage.getItem("token");
    var numnote = sessionStorage.getItem("numnote");
    if (!numnote) {
        if (date != "" && commentaire != "") {
            $.ajax({
                    method: "POST",
                    url: serveur + "notes/" + token,
                    data: {
                        "comment": commentaire,
                        "verrou": ver,
                        "date": date
                    }
                })
                .done(function (msg) {
                    if (msg["ret"] != "expired") {
                        message("ajoutnote");
                        var nb_note = msg["result"];
                        sessionStorage.setItem("numnote", nb_note);
                        ajoutline();
                        $("#envoi").hide();
                    } else if (msg["ret"] == "expired") {
                        expired();
                    }
                });
        } else {
            message("oublie");
        }
    }
}


function expired(msg) {
    if(msg!="pwd"){
        
    alert("session expirée !");

        
    }
    setTimeout(function () {
        location.reload();
    }, 1000);

}

function updatenotes() {
    var ver = $('#gridCheck:checked').val();
    if (ver) {
        ver = 1;
    } else {
        ver = 0;
    }

    //var token = sessionStorage.getItem("token");
    var numnote = sessionStorage.getItem("numnote");
    var date = $('#date').val();
    var commentaire =$('#commentaire').val();
    $.ajax({
            method: 'PUT',
            url: serveur + "notes/" + token + "/" + numnote + "?comment=" + commentaire + "&verrou=" + ver + "&date=" + date
        })
        .done(function (msg) {
            if (msg["ret"] == false) {
                expired();
            }
            if (msg["result"]) {
                message("modifnote");
            }

        });
}

function suprimer() {
    var delet = $(this).attr("id");
    if (delet != undefined) {
        $("." + delet).remove();
        //var token = sessionStorage.getItem("token");
        $.ajax({
                method: 'DELETE',
                url: serveur + "frais/" + token + "/" + delet
            })
            .done(function (msg) {
                message("sup");
                cal();
            });
    }
}

function ajoutline() {
    $(".test").append('<tr><th class="hidden-xs" scope="row">' + num +
        '</th><td><select class="form-control col-md-7 col-lg-7 formnote typedenote type"name="type"></select></td><td><input type="number" min="0.01" step="0.01"   class="form-control formnote montantnote" id="montantnote"/></td><td><input  class="form-control formnote commentaire" id="commentairenote"/></td> <td ><img src="images/bleach-sign.svg" class="delete"/></td></tr>');
    num++;
    montant = false;
    com = false;
    $(".type").remove("option");
    ajouttype();
    modiFrais();
}


function ajoutfrais(val) {
    var numfrais;
    var change = val.attr("id");
    var line = val.parent().parent();
    var fraismontant = line.find("#montantnote").val();
    var verif =fraismontant.match('[-+]?([0-9]*.[0-9]+|[0-9]+)');
    if(verif == null){
        fraismontant=undefined;
    }
    var fraiscommentaire = line.find("#commentairenote").val();
    var fraistype = line.find(".typedenote").val();
    var nb_note = sessionStorage.getItem("numnote");
    //var token = sessionStorage.getItem("token");
    var id = sessionStorage.getItem("id");
    var nb_note = sessionStorage.getItem("numnote");
    if(fraismontant != undefined){
    $.ajax({
            method: "POST",
            url: serveur + "frais/" + token,
            data: {
                "commentFrais": fraiscommentaire,
                "montantFrais": fraismontant,
                "idNote": nb_note,
                "idType": fraistype
            }
        })
        .done(function (msg) {
            if (msg["ret"] == false) {
                expired();
            } else {
                message("ajoutfrais");
                $(".type").addClass("ajouttype");
                $(".type").removeClass("type");
                line.find(".delete").attr('id', msg["result"]);
                line.attr('class', msg["result"]);
                ajoutline();
                cal();
            }
        });
    }

}
