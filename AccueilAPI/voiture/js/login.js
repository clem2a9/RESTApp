var loginToken = null;

function getToken() {
	return $.cookie("token");
	}
	
function destroyToken() {
	$.removeCookie("token");
	}

$( document ).ready(function() {
	if(getToken() == null)
		{
		$('#LoginPopUp').modal();  
		}
		else
		{
		loginToken = getToken();
		$.ajax({
			method: "GET",
			url: "http://chezmeme.com/sso/api/v1.6/check_access/" + loginToken
			})
		.done(function(data){ 
			if(data.ret == "denied")
				{
				detectError('AU001',$('#LoginPopUp'));
				}
			if(data.ret == "expired")
				{
				detectError('AU003',$('#LoginPopUp'));
				}
			});
		}
		
	$('#LoginPopUp .validButton').click(function() {
		login()
		});
		
	$('#LoginPopUp input').keypress(function(e) {
		if(e.charCode == 13)
			{
			login();
			}
		});
		
	$('.loginIcon').click(function() {
		logout();	
		});
		
	$('.textLogin').click(function() {
		logout();
		});
	});
	
function login() {
	var login = $('#LoginPopUp #login').val();
	var mdp = $('#LoginPopUp #mdp').val();
	if(login == "" || mdp == "")
		{
		$('#LoginPopUp').modal('hide'); 
		detectError('AU002',$('#LoginPopUp'));
		return;
		}
	$.ajax({
		method: "GET",
		url: "http://chezmeme.com/sso/api/v1.6/login/" + login + "/" + mdp
		})
	.done(function(data){ 
		$('#LoginPopUp').modal('hide'); 
		if(data.ret)
			{
			var token = data.token
			$.ajax({
				method: "GET",
				url: "http://chezmeme.com/sso/api/v1.6/check_access/" + token + "/vehicules"
				})
			.done(function(data){ 
				if(data.ret == "granted")
					{
					loginToken = token;
					$.cookie("token", token);
					$('#LoginPopUp #login').val('');
					$('#LoginPopUp #mdp').val('');
					}
				if(data.ret == "expired")
					{
					detectError('AU003',$('#LoginPopUp'));
					}
				});
			}
			else
			{
			detectError('AU002',$('#LoginPopUp'));
			}
		});
	}
	
function logout() {
	if(loginToken != null)
		{
		$.ajax({
			method: "GET",
			url: "http://chezmeme.com/sso/api/v1.6/logout/" + loginToken
			});
		loginToken = null;
		destroyToken();
		}
	$('#LoginPopUp').modal();  		
	}
