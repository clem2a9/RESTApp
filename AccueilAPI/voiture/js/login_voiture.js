var loginToken = null;

function getToken() {
	console.log (localStorage.getItem("token"));
	return localStorage.getItem("token");
}
	
function destroyToken() {
	sessionStorage.removeItem('token');
	}

$( document ).ready(function() {
	if(getToken() == null)
		{
		document.location.href="http://etudiant12.chezmeme.com/rest/"
		}
		else
		{
		loginToken = getToken();
		$.ajax({
			method: "GET",
			url: "http://chezmeme.com/sso/api/v1.6/check_access/" + loginToken + "/vehicules"
			})
		.done(function(data){ 
			if(data.ret == "granted")
				{
				loadVehiculesList();
				}
			if(data.ret == "denied")
				{
				detectError('AU001',$('#LoginPopUp'));
				}
			if(data.ret == "expired")
				{
				detectError('AU003',$('#LoginPopUp'));
				}
			});
		}
		
	$('.loginIcon').click(function() {
		logout();	
		});
		
	$('.textLogin').click(function() {
		logout();
		});
	});
	
function logout() {
	if(loginToken != null)
		{
		$.ajax({
			method: "GET",
			url: "http://chezmeme.com/sso/api/v1.6/logout/" + loginToken
			});
		loginToken = null;
		destroyToken();
		}
	document.location.href="http://etudiant12.chezmeme.com/rest/" 		
	}
