$( document ).ready(function() {
	var urlAPIVoiture = "http://etudiant12.chezmeme.com/res_voit/web/";
	$('#RecherchePopUp #validerRecherche').click(function () {
		var regex = /^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/;
		
		var dateDebut = $('#RecherchePopUp  #dateDeb').val();
		var dateFin = $('#RecherchePopUp  #dateFin').val();
		
		var demiJDebut = $('#RecherchePopUp #demi-journee1').val();
		var demiJFin = $('#RecherchePopUp #demi-journee2').val();
		
		var demiJFin = $('#RecherchePopUp #demi-journee2').val();
		
		var places = $('[name="gridRadios"]:checked').val();
		
		if(!regex.test(dateDebut))
			{
			$('#RecherchePopUp  #dateDeb').css('background-color','#EA9EB0');
			return;
			}
			
		if(!regex.test(dateFin))
			{
			$('#RecherchePopUp  #dateFin').css('background-color','#EA9EB0');
			return;
			}
		dateSave = DDMMYYYY_2_YYYYMMDD(dateDebut);
		if(demiJDebut == '00:00:00')
			{
			var djSave = '0';
			}
			else
			{
			var djSave = '1';
			}
		dateDebut = DDMMYYYY_2_YYYYMMDD(dateDebut) + ' ' + demiJDebut;
		dateFin = DDMMYYYY_2_YYYYMMDD(dateFin) + ' ' + demiJFin;
		
		$('#RecherchePopUp2 table tbody').html('');
		$('#RecherchePopUp2 .mobile').html('');
		var error = false;
		console.log(error);
		$.ajax({
			method: "GET",
			url: urlAPIVoiture + "vehicules/dispo/" + loginToken + "/",
			data : 'dateDebut=' + dateDebut + '&dateFin=' + dateFin + '&places=' + places
			})
		.done(function(data){ 
			if(detectError(data,$('#RecherchePopUp')))
				{
				$('#RecherchePopUp').modal('hide');
				return;
				}
			data.forEach(function (e) {
				$('#RecherchePopUp2 table tbody').append('<tr data-immat="' + e.immmatriculation + '" data-date="' + dateSave + '" data-dj="' + djSave + '"><td class="non-centre">' + e.marque + ' ' + e.modele + ' ' + e.couleur + '</td><td class="au-centre">' + e.immmatriculation + '</td><td class="au-centre">' + e.places + '</td><td class="au-centre"><img width="20px" height="20px" style="cursor:pointer;" src="https://useiconic.com/open-iconic/svg/external-link.svg"></td></tr>');
				
				$('#RecherchePopUp2 .mobile').append('<div data-immat="' + e.immmatriculation + '" data-date="' + dateSave + '" data-dj="' + djSave + '" class="card text-white bg-secondary centre recherche" style="max-width: 30rem;"><div class="card-header">' + e.marque + ' ' + e.modele + ' ' + e.couleur + '</div><div class="card-body"><div class="row"><div class="col card-text"><label>Immatriculation : </label> ' + e.immmatriculation + '</div></div><div class="row"><div class="col card-text"><label>Places : </label>' + e.places + '</div></div><div class="row"><div class="col card-text"><button type="button" class="btn btn-info">Réserver</button></div></div></div></div>');
				});
			if($('#RecherchePopUp2 table tbody tr').length == 0)
				{
				$('#RecherchePopUp2 table tbody').append('<tr><td colspan="4">Aucun véhicule n\'est disponible selon vos critères !</td></tr>');
				$('#RecherchePopUp2 .mobile').append('<div class="card text-white bg-secondary centre recherche" style="max-width: 30rem;"><div class="card-header">Aucun véhicule n\'est disponible selon vos critères !</div></div><div class="row"></div></div></div>');
				}
			loadRechercheListenner();
			$('#RecherchePopUp').modal('hide');
			$('#RecherchePopUp2').modal();
			});
		});
		
	$('#RecherchePopUp  #dateDeb').focus(function () {
		$('#RecherchePopUp  #dateDeb').css('background-color','#FFFFFF');
		});
	});
	
function loadRechercheListenner() {
	$('#RecherchePopUp2 table tbody img').click(function () {
		var vehicule = $(this).parent().parent().attr('data-immat');
		var date = $(this).parent().parent().attr('data-date');
		var demiJ = $(this).parent().parent().attr('data-dj');
		$('#RecherchePopUp2').modal('hide');
		
		$('.bojeu #vehicules').val(vehicule);
		$('.bojeu input').val(YYYYMMDD_2_DDMMYYYY(date));
		
		loadPlanning(vehicule,date,function() {
			openResaPopup('recherche',vehicule,date,demiJ);
			});
		});
	$('#RecherchePopUp2 .mobile button').click(function () {
		var vehicule = $(this).parent().parent().parent().parent().attr('data-immat');
		var date = $(this).parent().parent().parent().parent().attr('data-date');
		var demiJ = $(this).parent().parent().parent().parent().attr('data-dj');
		$('#RecherchePopUp2').modal('hide');
		
		$('.bojeu #vehicules').val(vehicule);
		$('.bojeu input').val(YYYYMMDD_2_DDMMYYYY(date));
		
		loadPlanning(vehicule,date,function() {
			openResaPopup('recherche',vehicule,date,demiJ);
			});
		});
	}
	
function DDMMYYYY_2_YYYYMMDD(date) {
	var noJour = date.substr(0,2);
	var noMois = date.substr(3,2);
	var noAnnee = date.substr(6,4);
	return noAnnee + '-' + noMois + '-' + noJour;
	}
	
function YYYYMMDD_2_DDMMYYYY(date) {
	var noAnnee = date.substr(0,4);
	var noMois = date.substr(5,2);
	var noJour = date.substr(8,4);
	return noJour + '/' + noMois + '/' + noAnnee;
	}
