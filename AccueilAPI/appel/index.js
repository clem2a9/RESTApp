var token = null;
var toedit = null;
$(document).ready(function () {
    $('#datetime').hide();
    /**
     *   GESTION DU TOKEN
     **/

    if (localStorage.getItem('token') != null) {
        Materialize.toast('Bienvenue', 4000)
        token = localStorage.getItem('token');
        $('#login-panel').hide();
        $('#corps-panel').show();
        afficheData();
    } else document.location.href = "../../index.html";

    /**
     *   CONNEXION
     **/

    $('#loginform').submit(function (event) {
        event.preventDefault();
        $.get("http://chezmeme.com/sso/api/v1.6/login/" + $('#username').val() + "/" + $('#password').val(), function () {})
            .done(function (data) {
                if (data.ret == 0) Materialize.toast('Identifiants incorrects', 4000)
                else {
                    Materialize.toast('Bienvenue', 4000)
                    token = data.token;
                    $('#login-panel').hide();
                    $('#corps-panel').show();
                    afficheData();
                }
            }).fail(function (data) {
                Materialize.toast('Erreur : Allow CORS', 4000)
            })

    });
    /**
     *   DECONNEXION
     **/
    $('#logout').click(function () {
        $.get("http://chezmeme.com/sso/api/v1.6/logout/" + localStorage.getItem('token'), function () {})
            .done(function (data) {
                Materialize.toast('Bienvenue', 4000)
                localStorage.removeItem('token');
                document.cookie = "token=null";
                document.location.href = "../../index.html";
            })
            .fail(function (data) {
                Materialize.toast('Erreur : No Internet OR Allow CORS', 4000)
            })
    });

    /**
     *   STYLE
     **/

    $('#callstatus').change(function () {
        if ($('#callstatus').val() == 1) $("#callcomment").show()
        else if ($('#callstatus').val() == 2) $("#callcomment").hide();
    });

    $('#checkdate').change(function () {
        if ($('#checkdate').is(":checked")) $('#datetime').hide();
        else {
            $('#datetime').show();
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1;
            var yyyy = today.getFullYear();
            if (dd < 10) dd = '0' + dd;
            if (mm < 10) mm = '0' + mm;
            var hour = "" + today.getHours();
            if (hour.length == 1) hour = "0" + hour;
            var minute = "" + today.getMinutes();
            if (minute.length == 1) minute = "0" + minute;
            var today = yyyy + '-' + mm + '-' + dd;
            $("#cdate").val(today);
            $("#ctime").val(hour + ":" + minute);
        }
    });

    /**
     *   RECHERCHE
     **/

    $('#recherche').keyup(function (e) {
        if ($('#recherche').val().length > 2) {
            $(".rsearch").each(function () {
                var tempfind = true;
                var tempid = $(this).attr('id');
                $("#" + tempid + " td").each(function () {
                    if ($(this).text().toLocaleLowerCase().includes($('#recherche').val().toLowerCase()) && tempfind) {
                        $(this).parent().show()
                        tempfind = false;
                    } else if (tempfind) $(this).parent().hide()
                });
            });
        } else if ($('#recherche').val() == "") {
            $("td").each(function () {
                $(this).parent().show()
            });
        }
    });
    $('#daterecherche').change(function (e) {
        if ($('#daterecherche').val() !== "") {

        }
    });
    /**
     *   DOWNLOAD
     **/
    $('#downloadlist').click(function () {
        var divContents = $("#testtable").html();
        var printWindow = window.open('', '', 'height=400,width=800');
        printWindow.document.write('<html><head><title>Liste en cours</title>');
        printWindow.document.write('</head><body >');
        printWindow.document.write(divContents);
        printWindow.document.write('</body></html>');
        printWindow.document.close();
        printWindow.print();
    });
    /**
     *   CHANGEMENT DE STATUT
     **/
    $("body").on('click', 'td a', function () {
        toedit = $(this).attr("id");
        toedit = toedit.slice(1);

    })
    $('#changeStatus').click(function (event) {

        if ($('#callstatus').val() != "") {
            /*var datasend = {};
            datasend.i_index = toedit;
            datasend.i_statut = $('#callstatus').val();
            datasend.s_commentaire = $('#textarea1').val();
            console.log(datasend);*/
            $.ajax({
                url: 'http://54.36.54.146:8000/api/appels/' + token + '?i_index=' + toedit + '&i_statut=' + $('#callstatus').val() + '&s_commentaire=' + $('#textarea1').val(),
                type: 'PUT',
                data: {},
                success: function (data) {
                    if (data == 100) document.location.href = "../../index.html";
                    console.log(data)
                    afficheData();
                    Materialize.toast('Appel modifié', 4000)
                },
                error: function (xhr, textStatus, errorThrown) {
                    Materialize.toast('Erreur de modification', 4000)
                }
            })
        } else Materialize.toast('Paramètres incomplets', 4000)
    });
    /**
     *   AJOUT D'UN APPEL
     **/
    $('#addcall').submit(function (event) {
        event.preventDefault();


        if ($('#checkdate').is(":checked")) {

            var cd = new Date().toISOString().slice(0, 19).replace('T', ' ');


            $.post("http://54.36.54.146:8000/api/appels/" + token, {
                    d_date: fdata(),
                    s_object: $('#cobject').val(),
                    i_statut: "2",
                    i_contact: $('#ccontact').val(),
                    i_urgence: $('#curgence').val(),
                    i_type: $('#ctype').val(),
                    i_personne: $('#cpersonne').val()
                })
                .done(function (data) {
                    afficheData();
                    Materialize.toast('Appel ajouté', 4000)
                })
                .fail(function (data) {
                    Materialize.toast('Erreur : POST APPEL', 4000)
                });
        } else {
            if ($('#cdate').val() == "") Materialize.toast('Champ date incorrect', 4000)
            else if ($('#ctime').val() == "") Materialize.toast('Champ time incorrect', 4000)
            else {

                var cd = $('#cdate').val() + " " + $('#ctime').val();

                $.post("http://54.36.54.146:8000/api/appels/" + token, {
                        d_date: cd,
                        s_object: $('#cobject').val(),
                        i_statut: "2",
                        i_contact: $('#ccontact').val(),
                        i_urgence: $('#curgence').val(),
                        i_type: $('#ctype').val(),
                        i_personne: $('#cpersonne').val()
                    })
                    .done(function (data) {
                        if (data == 100) document.location.href = "../../index.html";
                        afficheData();
                        Materialize.toast('Appel ajouté', 4000)
                    })
                    .fail(function (data) {
                        Materialize.toast('Erreur : POST APPEL', 4000)
                    });
            }
        }
    });
    /**
     *   AJOUT D'UN CONTACT
     **/
    $('#addcontact').submit(function (event) {
        event.preventDefault();

        if ($('#cnumber').val().length < 10) Materialize.toast('Numéro de téléphonne incorrect', 4000)
        else {
            $.post("http://54.36.54.146:8000/api/contacts/" + token, {
                    s_name: $('#cname').val(),
                    s_telephone: $('#cnumber').val()
                })
                .done(function (data) {
                    afficheData();
                    Materialize.toast('Contact ajouté', 4000)
                })
                .fail(function (data) {
                    Materialize.toast('Erreur : POST CONTACT', 4000)
                });
        }
    });
    /**
     *   AFFICHAGE DES DONN2ES SELON LE MOIS
     **/
    $("#monthreq").click(function () {




        var url = "http://54.36.54.146:8000/api/appels/" + token + '/' + $("#month").val()
        $.get(url, function () {})
            .done(function (data) {
                if (data == 100) document.location.href = "../../index.html";
                $('#heretoput').empty();
                $('#heretoputmobile').empty();
                data.forEach(function (val) {

                    getContact(val.i_contact, function (callback) {
                        var cc = callback;
                        getStatus(val.i_statut, function (callback) {
                            var cs = callback;
                            if (val.i_statut == 1) var st = '<i class="small material-icons green-text">check</i>'
                            else var st = '<i class="small material-icons red-text">close</i>'
                            getUrgence(val.i_urgence, function (callback) {
                                var cu = callback;
                                getType(val.i_type, function (callback) {
                                    var ct = callback;
                                    getPersonne(val.i_personne, function (callback) {
                                        var cp = callback;

                                        var formatedDate = fdate(val.d_date)
                                        $('#heretoput').append('<tr  class="rsearch" id="l' + val.i_index + '">\
                                        <td>' + cp.s_name + '</td>\
                                        <td>' + cc.s_name + '</td>\
                                        <td>' + cc.s_telephone + '</td>\
                                        <td><span>' + formatedDate + '</span></td>\
                                        <td >' + st + '</td>\
                                        <td>' + cu.s_urgence + '</td>\
                                        <td>' + ct.s_type + '</td>\
                                        <td><a id="b' + val.i_index + '" class="btn-floating btn-small waves-effect waves-light teal tooltipped modal-trigger" data-position="top" data-delay="50" data-tooltip="Changer de statut" href="#modal1"><i class="material-icons" >edit</i></a></td>\
                                    </tr>');

                                        $('#heretoputmobile').append('<li>\
                                        <div class="collapsible-header">' + st + ' ' + cp.s_name + '</div>\
                                        <div class="collapsible-body white">\
                                            <p><b>Destinataire : </b>' + cp.s_name + '</p>\
                                            <p><b>Appelant : </b>' + cc.s_name + '</p>\
                                            <p><b>Numéro : </b>' + cc.s_telephone + '</p>\
                                            <p><b>Date : </b>' + formatedDate + '</p>\
                                            <p><b>Urgence : </b>' + cu.s_urgence + '</p>\
                                            <p><b>Type : </b>' + ct.s_type + '</p>\
                                            <div class="center-align">\
                                                <a id="m' + val.i_index + '" class="btn-floating btn-small waves-effect waves-light teal modal-trigger" href="#modal1"><i class="material-icons" >edit</i></a>\
                                            </div>\
                                        </div>\
                                    </li>');
                                    });
                                });
                            });
                        });
                    });
                });
            })
            .fail(function () {
                Materialize.toast('Erreur : GET APPELS', 4000)
            })

    })
    /**
     *   AFFICHAGE DES DONNEES (ALL)
     **/
    function afficheData() {
        var today = new Date();
        var mm = today.getMonth() + 1;
        if (mm < 10) mm = "0" + mm;
        var yyyy = today.getFullYear();
        $("#month").val(yyyy + "-" + mm);

        var url = "http://54.36.54.146:8000/api/appels/" + token + '/' + yyyy + "-" + mm
        $.get(url, function () {})
            .done(function (data) {
                $('#heretoput').empty();
                $('#heretoputmobile').empty();
                listContatcs();
                listPersonnes();
                listTypes();
                listUrgences();

                data.forEach(function (val) {

                    getContact(val.i_contact, function (callback) {
                        var cc = callback;
                        getStatus(val.i_statut, function (callback) {
                            var cs = callback;
                            if (val.i_statut == 1) var st = '<i class="small material-icons green-text">check</i>'
                            else var st = '<i class="small material-icons red-text">close</i>'
                            getUrgence(val.i_urgence, function (callback) {
                                var cu = callback;
                                getType(val.i_type, function (callback) {
                                    var ct = callback;
                                    getPersonne(val.i_personne, function (callback) {
                                        var cp = callback;

                                        var formatedDate = fdate(val.d_date)
                                        $('#heretoput').append('<tr  class="rsearch" id="l' + val.i_index + '">\
                                        <td>' + cp.s_name + '</td>\
                                        <td>' + cc.s_name + '</td>\
                                        <td>' + cc.s_telephone + '</td>\
                                        <td><span>' + formatedDate + '</span></td>\
                                        <td >' + st + '</td>\
                                        <td>' + cu.s_urgence + '</td>\
                                        <td>' + ct.s_type + '</td>\
                                        <td><a id="b' + val.i_index + '" class="btn-floating btn-small waves-effect waves-light teal tooltipped modal-trigger" data-position="top" data-delay="50" data-tooltip="Changer de statut" href="#modal1"><i class="material-icons" >edit</i></a></td>\
                                    </tr>');

                                        $('#heretoputmobile').append('<li>\
                                        <div class="collapsible-header">' + st + ' ' + cp.s_name + '</div>\
                                        <div class="collapsible-body white">\
                                            <p><b>Destinataire : </b>' + cp.s_name + '</p>\
                                            <p><b>Appelant : </b>' + cc.s_name + '</p>\
                                            <p><b>Numéro : </b>' + cc.s_telephone + '</p>\
                                            <p><b>Date : </b>' + formatedDate + '</p>\
                                            <p><b>Urgence : </b>' + cu.s_urgence + '</p>\
                                            <p><b>Type : </b>' + ct.s_type + '</p>\
                                            <div class="center-align">\
                                                <a id="m' + val.i_index + '" class="btn-floating btn-small waves-effect waves-light teal modal-trigger" href="#modal1"><i class="material-icons" >edit</i></a>\
                                            </div>\
                                        </div>\
                                    </li>');
                                    });
                                });
                            });
                        });
                    });
                });
            })
            .fail(function (data) {
                Materialize.toast('Erreur : GET APPELS', 4000)
            })
        /**
         *   FUNTIONS DE CHARGEMENT DES DONNES BDD SELECTS
         **/
        function listContatcs() {
            $.get("http://54.36.54.146:8000/api/contacts/" + token, function () {})
                .done(function (data) {
                    if (data == 100) document.location.href = "../../index.html";
                    $('#ccontact').empty();
                    data.forEach(function (val) {
                        $('#ccontact').append('<option value="' + val.i_index + '">' + val.s_name + ' - ' + val.s_telephone + '</option>');
                        $('select').material_select()
                    });
                })
                .fail(function (data) {
                    Materialize.toast('Erreur : GET CONTACT', 4000)
                })
        }
        /* *************************** */
        function listUrgences() {
            $.get("http://54.36.54.146:8000/api/urgences/" + token, function () {})
                .done(function (data) {
                    $('#curgence').empty();
                    data.forEach(function (val) {
                        $('#curgence').append('<option value="' + val.i_index + '">' + val.s_urgence + '</option>');
                        $('select').material_select()
                    });
                })
                .fail(function (data) {

                    Materialize.toast('Erreur : GET URGENCES', 4000)
                })
        }
        /* *************************** */
        function listTypes() {
            $.get("http://54.36.54.146:8000/api/types/" + token, function () {})
                .done(function (data) {
                    if (data == 100) document.location.href = "../../index.html";
                    $('#ctype').empty();
                    data.forEach(function (val) {
                        $('#ctype').append('<option value="' + val.i_index + '">' + val.s_type + '</option>');
                        $('select').material_select()
                    });
                })
                .fail(function (data) {
                    Materialize.toast('Erreur : GET TYPES', 4000)
                })
        }
        /* *************************** */
        function listPersonnes() {
            $.get("http://54.36.54.146:8000/api/personnes/" + token, function () {})
                .done(function (data) {
                    $('#cpersonne').empty();
                    data.forEach(function (val) {
                        $('#cpersonne').append('<option value="' + val.i_index + '">' + val.s_name + '</option>');
                        $('select').material_select()
                    });
                })
                .fail(function (data) {
                    Materialize.toast('Erreur : GET PERSONNES', 4000)
                })
        }
    }

    /* GETTEURS & FONCTIONS*/
    function getType(id, callback) {
        $.get("http://54.36.54.146:8000/api/types/" + token + '/' + id, function () {})
            .done(function (data) {
                if (data == 100) document.location.href = "../../index.html";
                callback(data[0]);
            })
            .fail(function (data) {
                callback(data);
            })
    }

    function getUrgence(id, callback) {
        $.get("http://54.36.54.146:8000/api/urgences/" + token + '/' + id, function () {})
            .done(function (data) {
                callback(data[0]);
            })
            .fail(function (data) {
                callback(data);
            })
    }

    function getContact(id, callback) {
        $.get("http://54.36.54.146:8000/api/contacts/" + token + '/' + id, function () {})
            .done(function (data) {
                if (data == 100) document.location.href = "../../index.html";
                callback(data[0]);
            })
            .fail(function (data) {
                callback(data);
            })
    }

    function getPersonne(id, callback) {
        $.get("http://54.36.54.146:8000/api/personnes/" + token + '/' + id, function () {})
            .done(function (data) {
                if (data == 100) document.location.href = "../../index.html";
                callback(data[0]);
            })
            .fail(function (data) {
                callback(data);
            })
    }

    function getStatus(id, callback) {
        $.get("http://54.36.54.146:8000/api/statuts/" + token + '/' + id, function () {})
            .done(function (data) {
                if (data == 100) document.location.href = "../../index.html";
                callback(data[0]);
            })
            .fail(function (data) {
                callback(data);
            })
    }
    /**
     *   AUTRE
     **/
    function fdate(date) {
        var today = new Date(date) || new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        if (dd < 10) dd = '0' + dd;
        if (mm < 10) mm = '0' + mm;
        var hour = "" + today.getHours();
        if (hour.length == 1) {
            hour = "0" + hour;
        }
        var minute = "" + today.getMinutes();
        if (minute.length == 1) {
            minute = "0" + minute;
        }
        today = yyyy + '-' + mm + '-' + dd;
        return today + " " + hour + ":" + minute;
    }

    function fdata() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        if (dd < 10) dd = '0' + dd;
        if (mm < 10) mm = '0' + mm;
        var hour = "" + today.getHours();
        if (hour.length == 1) {
            hour = "0" + hour;
        }
        var minute = "" + today.getMinutes();
        if (minute.length == 1) {
            minute = "0" + minute;
        }
        today = yyyy + '-' + mm + '-' + dd;
        return today + " " + hour + ":" + minute;
    }

})

