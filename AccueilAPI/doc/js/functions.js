const CHEZMEME_BASE_URL = "http://chezmeme.com/sso/api/v1.6/";
const BASE_DOC_BASE_URL = "http://etudiant12.chezmeme.com/base_documentaire/public/api/";

var inactivityTimeout;

function uploadFile(token, formData) {
	$.ajax({
		url: BASE_DOC_BASE_URL + token + "/files",
		type: 'POST',
		data: formData,
		success: function(data) {
			console.log('FILES APP : Upload done !'),
			$('#uploadStatut').html('<br /><div class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">  L\'upload a été réalisé avec succès ! <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"> <span aria-hidden=\"true\">&times;</span> </button></div>'),
			$('#uploadForm')[0].reset(),
			$('#texteFile').html('Déposez votre fichier ou cliquez pour sélectionner	<br /> <i class=\"fa fa-cloud-upload\" aria-hidden=\"true\" style=\"font-size:48px;\"></i>');
		},
		error: function(data){
			console.log('FILES APP : Upload fail'),
			$('#uploadStatut').html('<br /><div class=\"alert alert-danger alert-dismissible fade show\" role=\"alert\">  L\'upload n\'a pas fonctionné ! <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"> <span aria-hidden=\"true\">&times;</span> </button></div>'),
			$('#uploadForm')[0].reset(),
			$('#texteFile').html('Déposez votre fichier ou cliquez pour sélectionner	<br /> <i class=\"fa fa-cloud-upload\" aria-hidden=\"true\" style=\"font-size:48px;\"></i>');
		},
		cache: false,
		contentType: false,
		processData: false
	});
}
// Uncomment the following to remove console logs in production
// console.log = function() {};

/**
 * Display page specified as parameter.
 * @param {page} : id of the wanted page.
 */
function display(page) {
  console.log('FILES APP : function display(page)');
  $('#uploadForm')[0].reset(),
  $('.active.page').addClass('d-none');
  $('.active.page').removeClass('active');
  $('#' + page).addClass('active');
  $('#' + page).removeClass('d-none');
  if (page != "login") {
    $('#header').removeClass('d-none');
    $('#header').addClass('active');
  }
}

/**
 * Load files from API "Base Documentaire".
 * @param {token} : Authenticated user token.
 */
function loadFiles(token) {
  console.log('FILES APP : function loadFiles(token)');
  // Call API to list files
  $.ajax({
      method: "GET",
      url: BASE_DOC_BASE_URL + token + "/files",
      dataType: "json"
    })
    .done(function (filesData) {
      displayFiles(filesData);
    })
    .fail(function () {
      console.log("FILES APP : file list not loaded. An error occured.");
    });
}

/**
 * Load form content from API "Base Documentaire".
 * @param {token} : Authenticated user token.
 */
function loadTypes(token) {
  console.log('FILES APP : function loadFormContent(token)');
  // Call API
  $.ajax({
      method: "GET",
      url: BASE_DOC_BASE_URL + token + "/files/types",
      dataType: "json"
    })
    .done(function (filesData) {
      displayTypes(filesData);
    })
    .fail(function () {
      console.log("FILES APP : types list not loaded. An error occured.");
    });
}

/**
 * Log out of app. This will log user out of ChezMeme's SSO destroying the token.
 */
function logout() {
  console.log('FILES APP : function logout');
  $.ajax({
      url: CHEZMEME_BASE_URL + "logout/" + localStorage.getItem("token"),
      dataType: "json"
    })
    .done(function (logoutData) {
      localStorage.removeItem('token');
      window.location.replace("http://etudiant12.chezmeme.com/rest/");

      $(document).unbind('mousemove keyup keypressed', function () {
        console.log('FILES APP : Unbinded event used to set timeout');
      });
    })
    .fail(function () {
      console.log("FILES APP : Logout failed");
    });
}

/**
 * Check user activity on app.
 * @param {timeout} : Timeout used to handle inactivity.
 */
function checkUserActivity(timeout) {
  console.log('FILES APP : function checkUserActivity');
  $.ajax({
      url: CHEZMEME_BASE_URL + "who_is/" + localStorage.getItem("token"),
      dataType: "json"
    })
    .done(function (checkUserTokenData) {
      console.log('FILES APP : check token done');
      var isTokenValid = checkUserTokenData.ret === 1 ? true : false;
      if (isTokenValid) {
        console.log('FILES APP : token is valid');
        timeout = setTimeout(function () {
          checkUserActivity(timeout);
        }, 1000 * 60 * 30);
      } else if (!isTokenValid) {
        console.log('FILES APP : token is not valid');
        logout();
      }
    })
    .fail(function (data) {
      console.log('FILES APP : check token failed');
    });
}

/**
 * Display files as a list.
 * @param {filesData} : Data from API.
 */
function displayFiles(data) {
  console.log('FILES APP : function displayFiles(data)');
  console.log(data);
  for (var i = 0, len = data.length; i < len; i++) {
    var title = data[i].hasOwnProperty('title') && data[i].title != null ? data[i].title : "<i class='fa fa-question-circle'></i>";
    var description = data[i].hasOwnProperty('description') && data[i].description != null ? data[i].description : "<i class='fa fa-question-circle'></i>";
    var id = data[i].hasOwnProperty('id') && data[i].id != null ? data[i].id : "<i class='fa fa-question-circle'></i>";
    var last_file = data[i].hasOwnProperty('last_file') && data[i].last_file != null ? data[i].last_file : "<i class='fa fa-question-circle'></i>";
    var user, first_name, type, last_name, path, version, fileDate, frenchDate;
    if (last_file != "") {
      type = last_file.hasOwnProperty('type') && last_file.type != null ? last_file.type : "<i class='fa fa-question-circle'></i>";
      user = last_file.hasOwnProperty('user') && last_file.user != null ? last_file.user : "<i class='fa fa-question-circle'></i>";
      first_name = user.hasOwnProperty('first_name') && user.first_name != null ? user.first_name : "<i class='fa fa-question-circle'></i>";
      last_name = user.hasOwnProperty('last_name') && user.last_name != null ? user.last_name : "<i class='fa fa-question-circle'></i>";
      path = last_file.hasOwnProperty('path') && last_file.path != null ? last_file.path : "<i class='fa fa-question-circle'></i>";
      version = last_file.hasOwnProperty('version') && last_file.version != null ? last_file.version : "<i class='fa fa-question-circle'></i>";
      // Apply deposit date to the Date function to format date
      fileDate = last_file.hasOwnProperty('deposit_date') ? new Date(last_file.deposit_date.date) : new Date();
      frenchDate = new Intl.DateTimeFormat("fr-FR");
      fileDate = frenchDate.format(fileDate);
    } else {
      first_name = '', last_name = '', path = '', version = '', fileDate = '';
    }

    $("table tbody").append(
      '<tr class="row">' +
      '<td class="col-1"> ' + title + ' </td>' +
      '<td class="col-2"> ' + first_name + ' ' + last_name + '</td>' +
      '<td class="col-2"> ' + description + ' </td>' +
      '<td class="col-2"> ' + type + ' </td>' +
      '<td class="col-1"> ' + version + ' </td>' +
      '<td class="col-2"> ' + fileDate + ' </td>' +
      '<td class="col-2 actions">' +
      '<a href="' + path + '" download=" ' + title + ' " class="btn btn-outline-success btn-block" data-toggle="tooltip" data-placement="right" title="Télécharger le fichier"><i class="fa fa-download"></i> Télécharger</a>' +
      '<a href="#" class="updateFile btn btn-outline-primary btn-block" data-toggle="tooltip" data-placement="right" title="Importer une version" data-link="' + data[i].show + '"><i class="fa fa-upload"></i> Modifier</a>' +
      '</td>' +
      '</tr>'
    );
    $("#accordion").append(
      '<div class="card">' +
      '<div class="bg-light card-header" id="heading' + i + '">' +
      '<h5 class="mb-0">' +
      '<div class="row p-2" data-toggle="collapse" data-target="#collapse' + i + '" aria-expanded="true" aria-controls="collapse' + i + '">' +
      '<span class="col-10">' + title + '</span>' +
      '<i class="text-primary col-2 fa fa-chevron-circle-down"></i>' +
      '</div>' +
      '</h5>' +
      '</div>' +
      '<div id="collapse' + i + '" class="collapse" aria-labelledby="heading' + i + '" data-parent="#accordion">' +
      '<div class="card-body">' +
      '<div class="row">' +
      '<div class="col-6 font-weight-bold">Propriétaire : </div><div class="col-6">' + first_name + ' ' + last_name + '</div>' +
      '</div>' +
      '<div class="row">' +
      '<div class="col-6 font-weight-bold">Description : </div><div class="col-6">' + description + '</div>' +
      '</div>' +
      '<div class="row">' +
      '<div class="col-6 font-weight-bold">Type de fichier : </div><div class="col-6">' + type + '</div>' +
      '</div>' +
      '<div class="row">' +
      '<div class="col-6 font-weight-bold">Version : </div><div class="col-6">' + version + '</div>' +
      '</div>' +
      '<div class="row">' +
      '<div class="col-6 font-weight-bold">Date de dépôt : </div><div class="col-6">' + fileDate + '</div>' +
      '</div>' +
      '<div class="row">' +
      '<a href="' + path + '" download=" ' + title + ' " class="btn btn-outline-success btn-block" data-toggle="tooltip" data-placement="bottom" title="Télécharger le fichier"><i class="fa fa-download"></i> Télécharger</a>' +
      '<a href="#" class="updateFile btn btn-outline-primary btn-block" data-toggle="tooltip" data-placement="bottom" title="Importer une version"><i class="fa fa-upload"></i> Modifier</a>' +
      '</div>' +
      '</div>' +
      '</div>' +
      '</div>'
    );

    // Add new version of a file
    $('.updateFile').unbind().on('click', function (e) {
      e.preventDefault();
      console.log('data-link : ' + $(this).attr('data-link'));
      var link = $(this).attr('data-link');
      loadFile(link);
      display('upload');
      // Fill the form with file informations
      $('#uploadForm').submit(function (e) {
        e.preventDefault();
        console.log('test');
      });
    });
  }
  console.log("FILES APP : Files list loaded");
}

/**
 * Display types in formhtml
 * @param {json} typesData 
 */
function displayTypes(typesData) {
  console.log('FILES APP : function displayTypes(typesData)');
  var types = '';
  for (var i = 0; i < typesData.length; i++) {
    var element = typesData[i];
    types += element.type + ' ';
    $('#fileType').append('<option value="' + element.id + '">' + element.type + '</option>');
  }
  console.log(types);
}

/**
 * 
 * @param {string} link 
 */
function loadFile(link) {
  console.log('FILES APP : function loadFile(link)');
  $.ajax({
      url: link,
      dataType: "json"
    })
    .done(function (fileData) {
      console.log(fileData);
      fillForm(fileData);
    })
    .fail(function () {
      console.log('FILES APP : error getting fileData');
    });
}

function fillForm(data) {
  var form = $('#uploadForm');
  console.log('FILES APP : function fillForm(data)', form.children('#title').val(data.title));
  if(data.title != null) $('#title').val(data.title);
  if(data.description != null) $('#description').val(data.description);
  if(data.has_pin) $('#pin').val(data.pin);
}

//code for adding keywords input
$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
   
    var x = 2; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div><input type="text" class="form-control" name="keywords[]" value="" placeholder="Mots clés..." /><i class="add_field_button fa fa-times remove_field keywordsDelete" aria-hidden="true" ></i><br /></div>'); //add input box
        }
    });
   
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});
	
