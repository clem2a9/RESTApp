// When DOM is loaded
$(document).ready(function () {

  // Upload
  var form = $("#uploadForm");
  var token = localStorage.getItem('token');
  form.on('submit', function (e) {
    e.preventDefault();
    var formData = new FormData(form[0]);
    uploadFile(token, formData)
  });

  // Display upload box    

  $('#openUpload').on('click', function () {
    display('upload');
  });

  $('#closeUpload').on('click', function () {
    display('index');
  });


  // AFFICHAGE DU NOM DU DOCUMENT LORS DU DRAG AND DROP
  $('#file').change(function (e) {
    var fileName = e.target.files[0].name;
    $('#texteFile').html(fileName + '<br /><i class=\"fa fa-paper-plane\" aria-hidden=\"true\" style=\"font-size:35px;\"></i>');
    //CHARGEMENT DU NOM DU DOC SI VIDE
    if ($('#title').val() == "") {
      $('#title').val(fileName);
    }
  });

  console.log('FILES APP : ready');
  // If the token is stored
  if (localStorage.getItem('token')) {
    $.ajax({
        url: CHEZMEME_BASE_URL + "who_is/" + localStorage.getItem('token'),
        dataType: "json"
      })
      .done(function (loginData) {
        if (loginData.ret == 0) {
          logout();
        }
      })
      .fail(function () {
        console.log('FILES APP : filtered research failed');
      });
    console.log('FILES APP : got token - ' + localStorage.getItem('token'));
    display('index');
    loadFiles(localStorage.getItem('token'));
    loadTypes(localStorage.getItem('token'));

    // Set timeout to check user inactivity
    $(document).on('mousemove keyup keypress', function () {
      clearTimeout(inactivityTimeout);
      //do any process and then call the function again
      inactivityTimeout = setTimeout(function () {
        checkUserActivity(inactivityTimeout);
      }, 1000 * 60 * 30); // Check token every 5 minutes
    });
  } else {
    logout();
  }

  // Display custom tootlips
  $('[data-toggle="tooltip"]').tooltip();

  // Prevent default behavior on click
  $("#btnFilters").on('click', function (e) {
    e.preventDefault();
  });

  // On logout, go to portal
  $("#btnLogout").on('click', function () {
    window.history.back();
  });

  $("form.search input").on('input change keypress keyup', function () {
    $('#keywords').val($(this).val());
    $('#description').val($(this).val());
  });

  // On date filter custom value, show date picker
  $(function () {

    var start = moment.unix(0),
      end = moment();

    function cb(start, end) {
      console.log('start:' + start, 'end:' + end);
      if (start <= 0) {
        $('#reportrange input#rangeStart').val(null);
        $('#reportrange input#rangeEnd').val(null);
        $('#reportrange input#fileDeposit').val('Tout');
      } else {
        $('#reportrange input#rangeStart').val(start.unix());
        $('#reportrange input#rangeEnd').val(end.unix());
        $('#reportrange input#fileDeposit').val(start.format('DD/MM/YY') + ' - ' + end.format('DD/MM/YY'));
      }
    }

    $('#reportrange').daterangepicker({
      startDate: start,
      endDate: end,
      ranges: {
        'Tout': [moment.unix(0), moment()],
        '7 derniers jours': [moment().subtract(6, 'days'), moment()],
        '30 derniers jours': [moment().subtract(29, 'days'), moment()],
        'Ce mois ci': [moment().startOf('month'), moment().endOf('month')],
        'Le mois dernier': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
        'Cette année': [moment().startOf('year'), moment().endOf('year')],
        'L\'année dernière': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
      }
    }, cb);

    cb(start, end);
  });

  // On submit of filter form, prevent default behavior
  $('#filterForm').submit(function (e) {
    e.preventDefault();
    console.log('FILES APP : filtered research submitted');

    var params = {
      'type': $('#fileType').val(),
      'size': $('#fileSize').val(),
      'rangeStart': $('#rangeStart').val() != $('#rangeEnd').val() ? $('#rangeStart').val() : null,
      'rangeEnd': $('#rangeEnd').val() != $('#rangeStart').val() ? $('#rangeEnd').val() : null,
      'keywords': $('#keywords').val().split(' ').join('|'),
      'description': $('#description').val().split(' ').join('|')
    };
    var data = '';

    for (var key in params) {
      if (params.hasOwnProperty(key)) {
        element = params[key];
        if (element !== '' && element !== undefined && element !== 'undefined') data != '' ? data += '&' + key + '=' + element : data += '' + key + '=' + element;
      }
    }

    console.log(data);

    $.ajax({
        url: BASE_DOC_BASE_URL + localStorage.getItem('token') + "/files?" + data,
        dataType: "json"
      })
      .done(function (filterFormData) {
        console.log('FILES APP : filtered research done');
        displayFiles(filterFormData);
      })
      .fail(function () {
        console.log('FILES APP : filtered research failed');
      });
  });

  $('form.search').submit(function (e) {
    e.preventDefault();
    $.ajax({
        url: BASE_DOC_BASE_URL + localStorage.getItem('token') + "/files?" + $('#searchInput').val(),
        dataType: "json"
      })
      .done(function (filterFormData) {
        console.log('FILES APP : filtered research done');
        displayFiles(filterFormData);
      })
      .fail(function () {
        console.log('FILES APP : filtered research failed');
      });
  });

});