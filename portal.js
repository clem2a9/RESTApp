$(document).ready(function () {
   
   if (!(localStorage.getItem('token') == null)) {
        $('#login-panel').hide();
        $('#corps-panel').show();
   }
    
    
    /* CONNEXION */ 
    $('#loginform').submit(function(event) {
        event.preventDefault();
        $.get( "http://chezmeme.com/sso/api/v1.6/login/"+$('#username').val()+"/"+$('#password').val(), function() {})
        .done(function(data) {
            if(data.ret == 0) Materialize.toast('Identifiants incorrects', 4000)
            else {
                Materialize.toast('Bienvenue', 4000)
//                console.log(data.token)
                localStorage.setItem('token', data.token);
//                console.log(localStorage.getItem('token'));
                $('#login-panel').hide();
                $('#corps-panel').show();
            }
        })
        .fail(function(data) {
            Materialize.toast('Erreur : No Internet OR Allow CORS', 4000)
            Materialize.toast(data, 4000)
        })
    });
    /* DECONNEXION */
    $('#logout').click(function() {
        $.get( "http://chezmeme.com/sso/api/v1.6/logout/"+localStorage.getItem('token'), function() {})
        .done(function(data) {
            Materialize.toast('Bienvenue', 4000)
            localStorage.removeItem('token');
            document.cookie = "token=null";
            location.reload();
        })
        .fail(function(data) {
            Materialize.toast('Erreur : No Internet OR Allow CORS', 4000)
            Materialize.toast(data, 4000)
        })
    });

});
